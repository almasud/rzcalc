/*************************************************
**************************************************
*** This Plugin developed by Md. Rashiduzzaman
*** Author Name 	: Rashid
*** Author  URL 	: http://linkinzone.com/;
*** Creating Date 	: 16-DEC-2017
***	©copyright www.linkinzone.com
**************************************************
**************************************************/

(function ( $ ) {
 var $idprefix = 'rz_';
 var $numCal=0;
$.fn.rzCalc = function(options) {
    var defaults = {
    	title: "Calculator",
    	titleBgColor: "#fff",
    	titleColor: "#000",

    	borderColor: "skyblue",
    	boxShadowColor:"#111",
        backgroundColor: "#fff",
        displayAreaBgColor:"#fff",

    	displayBgColor:"#fff",
    	//displayHoverBgColor:"#fff",
    	displayFontSize:"18px",

    	resultBgColor:"#fff",
    	//resultHoverBgColor:"#fff",
    	resultFontSize:"36px",

    	
    	buttonBgColor:"#eee",
    	buttonHoverBgColor:"#ccc",
    	buttonFontSize:"20px",
    	buttonTextColor:"#000",
    	buttonHoverTextColor:"#000",

    };
 
    var settings = $.extend( {}, defaults, options );
 
	RzCalc={
		num 	: "",
		nums    : [],
		datas	: null,
		display : " ",
		results : 0,
		operator: "",
		isPower : 0,
		isDot	: 0,
		isbOpen : 0,
		getEval : function(x){
			var $trim=this.num.trim();
			if($trim.split(' ').length>3 && this.isbOpen == 0){
				this.results=eval($trim.slice(0, -1));
				return this.results;
			}
		},
		
		getBack: function (){
			if(this.isPower>0){this.isPower=0;}
			if(this.isDot>0){this.isDot=0;}
			$num = this.num.trim().split(' ');
			$pop = $num.pop();
			
			if($pop.search('pow')!==-1 && $pop.slice(-1)=='*'){
				
				$base = $pop.trim().slice(9,-4);
				
			}else if($pop.search('pow')!==-1){
				
				$base = $pop.trim().slice(9,-3);
				
			}else if($pop.search('sqrt')!==-1 && $pop.slice(-1)=='*'){
				
				$base = $pop.trim().slice(10,-4);
				
			}else if($pop.search('sqrt')!==-1){
				
				$base = $pop.trim().slice(10,-3);
				
			}else{$base='';}
			
			$num = $num.join(' ');
			
			if($num.slice(-1)=='*'){
				$num = $num.slice(0,-1);
			}
			
			this.num = $num.trim()+$base;
			
			
			if(this.isBracket(this.display.trim().slice(-1)) && this.isbOpen > 0){
				this.isbOpen = this.isbOpen - 1;
			}
			this.display = this.display.trim().slice(0,-1);
		},
		getClear : function(){
			this.num 	= "";
			this.display = " ";
			this.results = 0;
			this.operator= "";
			this.isPower = 0;
			this.isDot	= 0;
			this.isbOpen = 0;
		},
		isOperator: function(x){
			if(x){
				return ['+','-','/','*','%','!','='].some(function(y){return y==x;});
			}else{
				return false;
			}
		},
		isBracket : function(x){
			if(x){
				return ['(','{','[',')','}',']'].some(function(y){return y==x;});
			}else{
				return false;
			}
		},
		isFactorial: function(n){
			if(n == 0 || n == 1){
				return 1;
			}else{
				return this.isFactorial(n-1) * n;
			}
		},
		
	};

	
	

	return this.each(function() {
		$numCal = $numCal + 1;
		var $this = RzCalc;
		$this.datas = $datas;
		$rzCalc = $( "<div class='calculatorArea rzcal"+$numCal+"'/>" ).appendTo(this);
		$rzCalc.append( getTemplate() );
		$('head').append("<style type='text/css'>"+getCss($numCal)+"</style>");

        $rzCalc.find('.buttonArea').on('click',function(e){
			$tgtClass	= e.target.className;
			$idx 		= e.target.dataset.rzidx;
			


			switch($tgtClass){
				case 'number':
					$num=$this.num.trim().split(' ').pop();
					if($num.search('pow')!=-1 || $num.search('sqrt')!=-1){
						
						$this.num = $this.num.trim()+'* '+$this.datas[$idx].value;
						
					}else{
						$this.num += $this.datas[$idx].value;
					}
					
					$this.display += $this.datas[$idx].symbol;
					
					break;
				case 'bracket':
					
					
					
					
					if($tgtClass=='bracketo'){
						
						$last=$this.num.trim().slice(-1);
						if(Number($last)){
							$this.num = $this.num.trim()+' *'+$this.datas[$idx].value+' ';
						}else{
							$this.num = $this.num.trim()+' '+$this.datas[$idx].value+' ';
						}
						
						
						
						$this.display +=' '+$this.datas[$idx].symbol;
						$this.isbOpen = $this.isbOpen + 1;
						
					}else{
						$this.num = $this.num.trim() + ' '+ $this.datas[$idx].value+' ';
						$this.display +=$this.datas[$idx].symbol+' ';
						
						$this.isbOpen = $this.isbOpen - 1;
					}
					

					
					break;
				case 'dot':
					
					if($this.isDot == 0){
						$this.num += $this.datas[$idx].value;
						$this.display += $this.datas[$idx].symbol;
						$this.isDot = $this.isDot + 1;
					}

					
					break;
				case 'pi':
					
					
					$this.num +=' *'+$this.datas[$idx].value;
					$this.display =$this.display.trim()+$this.datas[$idx].symbol+' ';
					

					
					break;
				case 'power':
				
					if($this.isDot>0){$this.isDot=0;}
					
					if($this.isPower==0){
						
						$this.display +=$this.datas[$idx].symbol+' ';
						
						$num = $this.num.trim().split(' ');
						$last = $num.pop();
						$this.num = $num.join(' ');
						$this.num +=' Math.pow('+$last+','+$this.datas[$idx].value+') ';
						
						$this.isPower = $this.isPower + 1;
					}
					
					break;
				case 'root':
					
					
						
						
						
						
						$num = $this.num.trim().split(' ');
						$last = $num.pop();
						if(Number($last)){
							$this.num = $num.join(' ');
							$this.num +=' Math.sqrt('+$last+') ';
							
							$display = $this.display.trim().split(' ');
							$last = $display.pop();
							$this.display = $display.join(' ');
							$this.display  += ' '+$this.datas[$idx].symbol+$last+' ';
						}
						
						
				
					break;
				case 'operator':
					
					if($this.isPower>0){$this.isPower=0;}
					if($this.isDot>0){$this.isDot=0;}
					
					$num = $this.num.trim();
					$display = $this.display.trim();
					
					if($this.isOperator($num.slice(-1))){
						$this.num = $num.slice(0,-1).trim();
						$this.display = $display.slice(0,-1).trim();
					}
					
					$this.num +=' '+$this.datas[$idx].value+' ';
					
					
					$this.display +=' '+$this.datas[$idx].symbol+' ';
					
					
					
					$this.getEval();
					
					break;
				case 'factorial':

					$num = $this.num.trim().split(' ');
					$last = $num.pop();
					$this.num = $num.join(' ')+' '+$this.isFactorial($last);
					$this.display +='!'+' ';


					break;
				case 'clear':
					
					$this.getClear();

					
					break;
				case 'back':
					
					$this.getBack();
					
					
					break;
				case 'equal':
				
					if($this.isbOpen == 0){
						$this.results = eval($this.num);
					}
					
					
					break;
				default:
					//code block
					
					
				
			}
			
			$(this).parent().find('.desplayPanel').html($this.display);
			$(this).parent().find('.resultPanel').text($this.results);
			console.log($this.num);
			console.log($this.results);
			
			
		});


        function getTemplate(){

			var $rzcalcTemplate = "";
			$rzcalcTemplate += "<div class='displayArea'>";
			$rzcalcTemplate += "<div class='displayTitle'>"+settings.title+"</div>";
			$rzcalcTemplate += "<div class='desplayPanel'>&nbsp;</div>";
			$rzcalcTemplate += "<div class='resultPanel'>0</div>";
			$rzcalcTemplate += "</div>";
			$rzcalcTemplate += "<table class='buttonArea'>";
			$rzcalcTemplate += "<tbody>";
				$rindex=0;
				$maxLoop = Math.ceil($datas.length/5);

				for($j=1;$j<=$maxLoop;$j++){
					$rzcalcTemplate +='<tr>';$td='';
					for($i=0;$i<5;$i++){
						/*$td +="<td><button id='";
								$td+=$idprefix+$datas[$rindex]['id'];
							$td +="' class='";*/
						

						$td +="<td><button class='";
								$td+=$datas[$rindex]['class'];
							$td +="' data-rzidx='"+$rindex+"' type='button'>";
							
								if($datas[$rindex]['id']=='square' || $datas[$rindex]['id'] =='quib'){
									$td +='X';
								}
								
								$td+=$datas[$rindex]['symbol'];
							$td +="</button></td>";

						$rindex++;
					}
					$rzcalcTemplate += $td+'</tr>';
					
					
				} 
			$rzcalcTemplate += "</tbody>";
			$rzcalcTemplate += "</table>";


			return $rzcalcTemplate;

		}

		function getCss($item){
			$myClass=".rzcal"+$item;
			$css=$myClass+"{"+
				"border:1px solid "+settings.borderColor+";"+
				"box-shadow:1px 3px 4px "+settings.boxShadowColor+";"+
			"}"+
			$myClass+" .displayArea{"+
				"background-color:"+settings.displayAreaBgColor+
			"}"+
			$myClass+" .displayTitle{"+
				"background-color:"+settings.titleBgColor+";"+
				"color:"+settings.titleColor+
			"}"+
			$myClass+" .displayPanel{"+
    			"background-color:"+settings.displayBgColor+";"+
    			"font-size:"+settings.displayFontSize+
			"}"+
			$myClass+" .resultPanel{"+
				"background-color:"+settings.resultBgColor+";"+
    			"font-size:"+settings.resultFontSize+
			"}"+
			$myClass+" table.buttonArea td button{"+
				"font-size:"+settings.buttonFontSize+";"+
				"background-color:"+settings.buttonBgColor+";"+
    			"color:"+settings.buttonTextColor+
			"}"+
			$myClass+" table.buttonArea td button:hover{"+
				"background-color:"+settings.buttonHoverBgColor+";"+
    			"color:"+settings.buttonHoverTextColor+
			"}";

			return $css;
		}

    });



};





var $datas=[
	{ 
		'value' : '2',
		'symbol': '²',
		'id'	: 'square',
		'class' : 'power'
	},
	{ 
		'value' : '3', 
		'symbol': '³',
		'id'	: 'quib',
		'class' : 'power'
	},
	{ 
		'value' : 'C', 
		'symbol': 'C',
		'id'	: 'clear',
		'class' : 'clear'
	},
	{ 
		'value' : 'B', 
		'symbol': '&larr;',
		'id'	: 'back',
		'class' : 'back'
	},
	{ 
		'value' : '/', 
		'symbol': '÷',
		'id'	: 'divde',
		'class' : 'operator'
	},
	{ 
		'value' : 'Math.PI', 
		'symbol': 'π',
		'id'	: 'pi',
		'class' : 'pi'
	},
	{ 
		'value' : '7', 
		'symbol': '7',
		'id'	: 'seven',
		'class' : 'number'
	},
	{ 
		'value' : '8', 
		'symbol': '8',
		'id'	: 'eight',
		'class' : 'number'
	},
	{ 
		'value' : '9', 
		'symbol': '9',
		'id'	: 'nine',
		'class' : 'number'
	},
	{ 
		'value' : '*', 
		'symbol': '×',
		'id'	: 'multipl',
		'class' : 'operator'
	},
	{ 
		'value' : '√', 
		'symbol': '√',
		'id'	: 'root',
		'class' : 'root'
	},
	{ 
		'value' : '4', 
		'symbol': '4',
		'id'	: 'four',
		'class' : 'number'
	},
	{ 
		'value' : '5', 
		'symbol': '5',
		'id'	: 'five',
		'class' : 'number'
	},
	{ 
		'value' : '6', 
		'symbol': '6',
		'id'	: 'six',
		'class' : 'number'
	},
	{ 
		'value' : '-', 
		'symbol': '-',
		'id'	: 'minus',
		'class' : 'operator'
	},
	{ 
		'value' : 'n!', 
		'symbol': 'n!',
		'id'	: 'factorial',
		'class' : 'factorial'
	},
	{ 
		'value' : '1', 
		'symbol': '1',
		'id'	: 'one',
		'class' : 'number'
	},
	{ 
		'value' : '2', 
		'symbol': '2',
		'id'	: 'two',
		'class' : 'number'
	},
	{ 
		'value' : '3', 
		'symbol': '3',
		'id'	: 'three',
		'class' : 'number'
	},
	{ 
		'value' : '+', 
		'symbol': '+',
		'id'	: 'plus',
		'class' : 'operator'
	},
	{ 
		'value' : '(', 
		'symbol': '(',
		'id'	: 'bracketo',
		'class' : 'bracket'
	},
	{ 
		'value' : ')', 
		'symbol': ')',
		'id'	: 'bracketc',
		'class' : 'bracket'
	},
	{ 
		'value' : '0', 
		'symbol': '0',
		'id'	: 'zero',
		'class' : 'number'
	},
	{ 
		'value' : '.', 
		'symbol': '.',
		'id'	: 'dot',
		'class' : 'dot'
	},
	{ 
		'value' : '=', 
		'symbol': '=',
		'id'	: 'equal',
		'class' : 'equal'
	},
	
];




}( jQuery ));
		